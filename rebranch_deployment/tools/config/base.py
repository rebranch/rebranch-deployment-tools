import os


class EnvConfigParserAbstract(object):
    env_variable_name = None
    _env_variable_value = None

    def __init__(self):
        assert self.env_variable_name is not None
        self._env_variable_value = os.getenv(self.env_variable_name, u'')

    @property
    def env_variable_value(self):
        return self._env_variable_value

    def get_config_object(self):
        raise NotImplementedError