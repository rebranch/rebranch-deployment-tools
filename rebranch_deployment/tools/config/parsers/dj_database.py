try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

from rebranch_deployment.tools.config.base import EnvConfigParserAbstract


class DjangoDatabaseConfigParser(EnvConfigParserAbstract):
    env_variable_name = u'DATABASE_URL'

    def get_config_object(self):
        """

        :rtype : dict
        """
        parsed_value = urlparse(self.env_variable_value)
        config_object = {
            u'ENGINE': parsed_value.scheme,
            u'NAME': parsed_value.path[1:],
            u'USER': parsed_value.username,
            u'PASSWORD': parsed_value.password,
            u'HOST': parsed_value.hostname,
            u'PORT': parsed_value.port,
        }
        return config_object