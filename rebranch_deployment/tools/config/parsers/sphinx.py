try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

from rebranch_deployment.tools.config.base import EnvConfigParserAbstract


class SphinxConfigParser(EnvConfigParserAbstract):
    env_variable_name = u'SPHINX_URL'

    def get_config_object(self):
        """
        @rtype: urlparse.urlparse.ParseResult
        """
        return urlparse(self.env_variable_value)