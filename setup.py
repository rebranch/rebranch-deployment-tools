from os.path import join, dirname

from setuptools import setup


setup(
    name='rebranch_deployment_tools',
    version=0.1,
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    author='Rebranch',
    install_requires=[u'dj-database-url==0.2.2'],
    url='https://Franz_ru@bitbucket.org/rebranch/rebranch-deployment-tools.git'
)